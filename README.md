# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/meef-math/planning) [Planning M1.MEEF Lille](http://meef-math.gitlabpages.univ-lille.fr/planning)
Les plannings du Master MEEF Mathématiques de l'université de Lille.

## 2024-2025
Les documents suivants contiennent le planning d'une semaine type. Pour connaître le « vrai » planning de la semaine il faut se référer aux documents Google dont le lien est disponible sur Moodle.

- [Semaine type S1](M1.MEEF-Planning-S1-2024-2025.pdf)
- [Semaine type S2](M1.MEEF-Planning-S2-2024-2025.pdf)
- [Semaine type S3](M2.MEEF-Planning-S3-2024-2025.pdf)
- [Semaine type S4](M2.MEEF-Planning-S4-2024-2025.pdf)
